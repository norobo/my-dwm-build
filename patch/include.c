/* Bar functionality */
#include "bar_indicators.c"
#include "bar_tagicons.c"

#include "bar_powerline_status.c"
#include "bar_powerline_tags.c"
#include "bar_statuscolors.c"
#include "bar_flexwintitle.c"
#include "bar_systray.c"
#include "bar_wintitleactions.c"

/* Other patches */
#include "attachx.c"
#include "cyclelayouts.c"
#include "rotatestack.c"
#include "scratchpad.c"
#include "swallow.c"
#include "vanitygaps.c"
#include "warp.c"
/* Layouts */
#include "layout_facts.c"
#include "layout_tile.c"
