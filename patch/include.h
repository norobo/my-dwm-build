/* Bar functionality */
#include "bar_indicators.h"
#include "bar_tagicons.h"

#include "bar_powerline_status.h"
#include "bar_powerline_tags.h"
#include "bar_flexwintitle.h"
#include "bar_systray.h"
#include "bar_wintitleactions.h"

/* Other patches */
#include "attachx.h"
#include "cyclelayouts.h"
#include "rotatestack.h"
#include "scratchpad.h"
#include "swallow.h"
#include "vanitygaps.h"
#include "warp.h"
/* Layouts */
#include "layout_tile.h"
